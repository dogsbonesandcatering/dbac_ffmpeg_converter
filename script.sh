#!/bin/bash

# Version: DEV

## VARIABLES
# Ordner finale Filme 
FINAL="/Volumes/Untitled/DBAC2023/FINAL"
# Ornder Temporäre Ablage
TEMP="/Volumes/Untitled/DBAC2023/TEMP"

if [[ $# -ne 5 ]] ; then
    echo 'Please provide proper Arguments: "script.sh #INPUTMOVIE #INPUTJPG #ABGABENUMMER #TEAMNAME #FILMTITEL"'
    read -p "Continue with dialog input? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
    # get variables in dialogue
    read -p "Path to Original Movie: " IN
    read -p "Path to Kachel Image: " INIMG
    read -p "Abgabenummer: " ANR
    read -p "Team-Name (no whitespace!!): " TEAM
    read -p "Filmtitel (no whitespace!! ): " MOVIE
fi

#input File ()
IN="$1"
# image of input movie
INIMG="$2"

# Anmeldenummer
ANR="$3"
# Teamname
TEAM="$4"
# Filmname
MOVIE="$5"
# output NAME
OUT=$ANR-$TEAM-$MOVIE

if [[ ! -f $IN ]]; then
    echo " Inputmovie: '$IN' does not exist. exit."
    exit 1
fi

if [[ ! -f $INIMG ]]; then
    echo " Inputpicture: '$INIMG' does not exist. exit."
    exit 1
fi

if [[ ! -d $TEMP ]]; then
    echo "TEMPDIR '$TEMP' not set properly. exit."
    exit 1
fi

if [[ ! -d $FINAL ]]; then
    echo "FINALDIR '$FINAL' not set properly. exit."
    exit 1
fi



# convert input into 25 pixel format
echo "converting input into 25 pixel format"
ffmpeg -r 25 -i "$IN" -c:v prores -profile:v 3 -vendor ap10 -pix_fmt yuv422p10le -vf scale=1920:1080 "$TEMP/$OUT.mov"

# create 5 seconfds long picture povie
echo "create 5 seconds long picture pre-moviefile"
ffmpeg -loop 1 -i "$INIMG" -c:v prores -t 5 -pix_fmt yuv422p10le -vf scale=1920:1080 "$TEMP/$OUT-img.mov"
#add silent audio:
ffmpeg -f lavfi -i anullsrc -i "$TEMP/$OUT-img.mov" -c:v copy -c:a aac -shortest "$TEMP/$OUT-img-a.mov"

# normalize the movie
echo "normalize movie"
ffmpeg-normalize "$TEMP/$OUT.mov" -ext mov -c:a aac -tp -5.0 -ar 44100 -o "$TEMP/$OUT-n.mov" -f

# now concat the kachel to the movie
echo "put all files together to final"
echo "file '$TEMP/$OUT-img-a.mov'" > files-to-combine.txt
echo "file '$TEMP/$OUT-n.mov'" >> files-to-combine.txt
ffmpeg -safe 0 -f concat -i files-to-combine.txt -vcodec copy -acodec copy "$FINAL/$OUT.mov"

echo "Done Movie $ANR: '$FINAL/$ANR/$OUT.mov'" 
